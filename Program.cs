using System.IO;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Extensions.Logging;
using Distributed.Common;

class Program
{
    static void Main(string[] args)
    {
        var collection = new ServiceCollection();
        var workingDir = Directory.GetCurrentDirectory();
        var config = new ConfigurationBuilder().SetBasePath(workingDir)
                .AddJsonFile("settings.json", optional: false, reloadOnChange: true).Build();
        
        Log.Logger = new LoggerConfiguration().Enrich.FromLogContext()
                .WriteTo.RollingFile(Path.Combine(workingDir, config.GetSection("LogFile").Value))
                .MinimumLevel.Debug().CreateLogger();

        var provider = collection
            .WithLoggerProvider(new SerilogLoggerProvider(Log.Logger))
            .WithConfig(config)
            .RegisterServiceDependencies(true)
            .BuildServiceProvider();
            
        var serviceLoop = provider.GetService<IServiceLoop>();

        var pubsub = provider.GetService<IPubSubMessenger>();
        var logger = provider.GetService<Microsoft.Extensions.Logging.ILogger>();

        pubsub.SubscribeAsync("TEST", (msg) => {
            var time = msg.CreationTime;
            logger.LogDebug($"[{time.Hour}:{time.Minute}:{time.Second}] Received {msg.Message}");
        }, true).Wait();

        pubsub.SubscribeAsync("TEST2", (msg) => {
            var time = msg.CreationTime;
            logger.LogDebug($"[{time.Hour}:{time.Minute}:{time.Second}] Received {msg.Message}");
        }).Wait();

        serviceLoop.RunAsync().Wait();
    }
}
