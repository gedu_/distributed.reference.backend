using System;
using System.Threading.Tasks;
using Distributed.Reference.Backend.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Reference.Backend
{
    public class ExampleService : IExampleService
    {

        public decimal Add(decimal first, decimal second)
        {
            return first + second;
        }

        public async Task DoSomething()
        {
            await Task.Delay(5000);
        }

        public string Greet()
        {
            return "Hello, Gedas";
        }

        public string Greet(string name)
        {
            return $"Hello, {name}";
        }
    }
}